﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;

namespace Pong
{
    public partial class Pong : Form
    {
        private const int padMovementSpeed = 4;
        private const int ballMovementSpeed = 4;
        int YAxisIncrement = 0;
        int ballDirectionXAxis = 1;
        int tick = 0;
        int scoreLeft = 0;
        int scoreRight = 0;

        public Pong()
        {
            InitializeComponent();
        }

        private void gameTick_Tick(object sender, EventArgs e)
        {
            MoveBall();
            CheckCollisions();
            CheckKeys();
            tick++;
            
        }

        private void CheckKeys()
        {
            if (Keyboard.IsKeyDown(Key.A))
            {
                MovePad(1, 0);
            }
            if (Keyboard.IsKeyDown(Key.Z))
            {
                MovePad(1, 1);
            }

            if (Keyboard.IsKeyDown(Key.K))
            {
                MovePad(0, 0);
            }
            if (Keyboard.IsKeyDown(Key.M))
            {
                MovePad(0, 1);
            }
        }

        private void CheckCollisions()
        {

            // Toca uno de los jugadores
            if (padLeft.Left == ball.Left)
            {
                for (int i = 0; i < padLeft.Height + ball.Height; i++)
                {

                    if (padLeft.Location.Y - ball.Height + i == ball.Location.Y)
                    {
                        ballDirectionXAxis = 0;
                        BallHit(i);
                    }
                }
            }

            if (padRight.Left == ball.Left)
            {
                for (int i = 0; i < padRight.Height + ball.Height; i++)
                {

                    if (padRight.Location.Y - ball.Height + i == ball.Location.Y)
                    {
                        ballDirectionXAxis = 1;
                        BallHit(i);
                    }
                }
            }
            // toca abajo (incremento positivo
            if (ball.Top > ClientSize.Height - ball.Height)
            {
                YAxisIncrement = YAxisIncrement - (YAxisIncrement*2);
            }
            //toca arriba (incremento negativo)
            if (ball.Top < 0)
            {
                YAxisIncrement = Math.Abs(YAxisIncrement);
            }

            // Toca en los lados
            if (ball.Left > ClientSize.Width)
            {
                ballDirectionXAxis = 1;
                scoreLeft += 1;
                ScoreLeft.Text = scoreLeft.ToString();
                Score();
            }

            if (ball.Left < 0)
            {
                ballDirectionXAxis = 0;
                scoreRight += 1;
                ScoreRight.Text = scoreRight.ToString();
                Score();
            }

        }

        private void Score()
        {
            if (scoreRight == 5)
            {
                Won("Gana el jugador 1");
                blackPicture.Show();
            } else if (scoreLeft == 5)
            {
                Won("Gana el jugador 2");
                gameTick.Stop();
                blackPicture.Show();
                ball.Top = ClientSize.Height / 2;
                ball.Left = ClientSize.Width / 2;
                YAxisIncrement = 0;
                padLeft.Top = (ClientSize.Height / 2) - (padLeft.Height / 2);
                padRight.Top = (ClientSize.Height / 2) - (padRight.Height / 2);
            } else
            {
                ball.Left = -100;
                Thread.Sleep(1000);
                ball.Top = ClientSize.Height / 2;
                ball.Left = ClientSize.Width / 2;
                YAxisIncrement = 0;
                padLeft.Top = (ClientSize.Height / 2) - (padLeft.Height / 2);
                padRight.Top = (ClientSize.Height / 2) - (padRight.Height / 2);
            }
        }

        private void Won(string text)
        {
            bigText.Show();
            blackPicture.Show();
            bigText.Text = text;
            Task.Delay(2000).Wait();
            bigText.Hide();
            LoadMenu();
        }

        private void BallHit(int i)
        {
            
            int increment = (i - ball.Height/2) - (padLeft.Height/2);
            YAxisIncrement = increment / 6;
        }

        private void MoveBall()
        {
            if (ballDirectionXAxis == 0)
            {
                ball.Top += YAxisIncrement;
                ball.Left += ballMovementSpeed;
            }
            if (ballDirectionXAxis == 1)
            {
                ball.Top += YAxisIncrement;
                ball.Left -= ballMovementSpeed;
            }

        }


        private void MovePad(int pad, int direction)
        {
            if (pad == 1)
            {
                if (direction == 0 && padLeft.Top > 0)
                {
                    padLeft.Top = padLeft.Top - padMovementSpeed;
                }
                if (direction == 1 && padLeft.Top < (this.ClientSize.Height - padLeft.Size.Height)) 
                {
                    padLeft.Top = padLeft.Top + padMovementSpeed;
                }
            }

            if (pad == 0)
            {
                if (direction == 0 && padRight.Top > 0)
                {
                    padRight.Top = padRight.Top - padMovementSpeed;
                }
                if (direction == 1 && padRight.Top < (this.ClientSize.Height - padRight.Size.Height))
                {
                    padRight.Top = padRight.Top + padMovementSpeed;
                }
            }
        }
        
        private void Form1_Load(object sender, EventArgs e)
        {
            this.BringToFront();
            this.Focus();
            this.KeyPreview = true;

        }


        private void LoadMenu()
        {
            blackPicture.Show();
            label2.Show();
            label1.Show();
            dot1.Show();
            dot2.Show();
            txtSalir.Show();
            gameTick.Stop();
        }

        private async Task LoadGameAsync()
        {
            // Reseteamos los valores de direccion de la bola
            YAxisIncrement = 0;
            ballDirectionXAxis = 1;

            // Reseteamos los contadores
            scoreLeft = 0;
            ScoreLeft.Text = scoreLeft.ToString();
            scoreRight = 0;
            ScoreRight.Text = scoreRight.ToString();

            // Ocultamos los elemenos de la interfaz del menu
            label2.Hide();
            label1.Hide();
            dot1.Hide();
            dot2.Hide();
            txtSalir.Hide();
            blackPicture.Hide();

            // Cuenta atras de 3 segundos.

            for (int i = 3; i > 0; i--)
            {
                countDown.Text = i.ToString();
                countDown.Show();
                
                for (int j = 11; j > 1; j--)
                {
                    countDown.Left = 290 - j * 7;
                    countDown.Width = 100;
                    await Task.Delay(100);
                    countDown.Font = new System.Drawing.Font(countDown.Font.Name, j*8, countDown.Font.Style);
                }
                countDown.Hide();
                
            }
            countDown.Hide();

            // Iniciamos el contador
            gameTick.Start();

        }

        private void label2_Click(object sender, EventArgs e)
        {
            LoadGameAsync();
        }

        private void txtSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
